/*
 * Copyright (c) 2014, TrueNull.se
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of TrueNull.se nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package se.truenull.school.traveltime;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.Stack;

public class TravelActivity extends ActionBarActivity {
    /**
     * The velocity to travel at (in km/h)
     */
    Integer speed;
    Integer distance;
    AdapterView.OnItemSelectedListener av;
    SeekBar.OnSeekBarChangeListener sb;
    View.OnTouchListener speedListener;
    Stack<Integer> distanceLog = new Stack<Integer>();

    public TravelActivity() {
        super();
        speed = 20;
    }

    @Override
    public void onBackPressed() {
        if (distanceLog.isEmpty()) {
            super.onBackPressed();
        } else {
            distance = distanceLog.pop();
            updateDistanceDisplay();
            calcTime();

            // Since the undo function is specified to do nothing on consecutive presses of
            // the back button.
            distanceLog.clear();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_travel);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment())
                    .commit();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        // Set spinners on item changed listener
        this.av = new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                this.setValue(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                this.setValue(0);
            }

            private void setValue(int index) {
                Resources res = getResources();
                int[] val = res.getIntArray(R.array.travel_spinner_values);
                speed = val[index];

                updateSpeedDisplay();
                calcTime();
            }
        };

        Spinner s = (Spinner) findViewById(R.id.travel_mode_spinner);
        s.setOnItemSelectedListener(this.av);

        // Set distance slider view update
        sb = new SeekBar.OnSeekBarChangeListener() {
            private int startDistance;

            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                distance = i;
                updateDistanceDisplay();
                calcTime();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                this.startDistance = distance;
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if (this.startDistance != distance) {
                    // distance has changed during touch
                    // Record old value for undo function
                    distanceLog.push(startDistance);
                }
            }
        };

        SeekBar seeker = (SeekBar) findViewById(R.id.travel_seekbar);
        seeker.setOnSeekBarChangeListener(sb);

        // manually call the event since it won't show the initial value otherwise.
        sb.onProgressChanged(seeker, seeker.getProgress(), false);

        // Implement temporary speed changing using touch events
        this.speedListener = new View.OnTouchListener() {
            float previousX;

            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getActionMasked() == MotionEvent.ACTION_DOWN) {
                    // Record previous X
                    previousX = motionEvent.getX();
                } else if (motionEvent.getActionMasked() == MotionEvent.ACTION_UP
                        || motionEvent.getActionMasked() == MotionEvent.ACTION_CANCEL) {
                    if (previousX < motionEvent.getX()) {
                        // Right
                        speed += 10;
                    } else {
                        // Left
                        speed -= 10;
                    }

                    speed = speed < 0 ? 0 : speed;

                    updateSpeedDisplay();
                    calcTime();
                }
                return true;
            }
        };

        View speedy = findViewById(R.id.travel_speed);
        speedy.setOnTouchListener(this.speedListener);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.travel, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void updateSpeedDisplay() {
        TextView speedView = (TextView) findViewById(R.id.travel_speed);
        speedView.setText(Integer.toString(speed) + " km/h");
    }

    public void updateDistanceDisplay() {
        TextView distanceView = (TextView) findViewById(R.id.travel_distance);
        distanceView.setText(Integer.toString(distance) + " km");
        SeekBar distanceBar = (SeekBar) findViewById(R.id.travel_seekbar);
        distanceBar.setProgress(distance);
    }

    public void calcTime() {
        Double time = (double) distance / (double) speed;
        TextView timeDisplay = (TextView) findViewById(R.id.travel_calculated_time);
        timeDisplay.setText(String.format("%.2f", time) + " h");
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_travel, container, false);
            return rootView;
        }
    }
}
